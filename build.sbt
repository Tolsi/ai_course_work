organization := "ru.tolsi"

name := "BlocksSolver"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "1.9.1" % "test"

libraryDependencies += "junit" % "junit" % "4.10" % "test"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)