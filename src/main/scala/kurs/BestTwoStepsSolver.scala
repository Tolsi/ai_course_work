package kurs

trait BestTwoStepsSolver extends Solver {

  def bestMove(moves: Seq[(Selection, (Field, Score))]): Option[(Selection, (Field, Score))] =
    moves match {
      case Nil => None
      case _ => {
        val twoMoves = for (move <- moves;
                            (selection, (field, score)) = move)
        yield if (!field.moves.isEmpty) {
            val bestOfNexts = field.moves.maxBy(_._2._2)
            (move, Some(bestOfNexts))
          }
          else
            (move, None)

        if (!twoMoves.isEmpty)
          Some(twoMoves.maxBy {
            case (firstMove, secondMove) => firstMove._2._2 + {
              secondMove match {
                case None => 0
                case Some(step) => step._2._2
              }
            }
          }._1)
        else
          None
      }
    }

  def from(initial: Stream[(List[Selection], (Field, Score))],
           explored: Set[Field]): Stream[(List[Selection], (Field, Score))] = {
    val (history, (field, score)) = initial.head
    val movesToUniqueStates = field.movesToUniqueStates
    val allNeighbors = explored ++ movesToUniqueStates.map(_._2._1)
    bestMove(movesToUniqueStates) match {
      case None => {
        initial
      }
      case Some(move) => {
        initial #::: from(Stream(moveWithHistory(move, initial.head)), allNeighbors)
      }
    }
  }

  lazy val allPaths = from(Stream((Nil, (start, 0))), Set(start))

  lazy val pathToGoal = allPaths.filter(p => done(p._2._1))

  lazy val solution: List[Selection] = pathToGoal.head._1
}
