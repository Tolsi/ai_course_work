package kurs

object ComparisonBlocksAlghs extends App {

  trait Level1 extends StringParserField {
    val field =
      """12122
        |21222
        |22122""".stripMargin
  }

  trait GameVisualizator extends Solver {
    def outState(field: Field, score: Score, moveInfo: String) = {
      println(s"$moveInfo Field: $field Score: $score")
    }

    def visualizeSolution = {
      outState(start, 0, "Start level field")
      solution.foldLeft((start, 0)) {
        case (fieldAndScore, selection) => {
          val move = fieldAndScore._1.select(selection)
          outState(move._1, fieldAndScore._2 + move._2, s"Move: $selection")
          (move._1, fieldAndScore._2 + move._2)
        }
      }
    }
  }

  trait BestStepLevel1Solver extends BestStepSolver with Level1 with GameVisualizator

  trait LengthLevel1Solver extends LengthSolver with Level1 with GameVisualizator

  trait WeightLevel1Solver extends WeightSolver with Level1 with GameVisualizator

  trait SmartWeightLevel1Solver extends SmartWeightSolver with Level1 with GameVisualizator

  new BestStepLevel1Solver {
    visualizeSolution
    println(s"BestStepSolver Search depth: ${allPaths.length}")
  }
  new LengthLevel1Solver {
    visualizeSolution
    println(s"LengthSolver Search depth: ${allPaths.length}")
  }
  new WeightLevel1Solver {
    visualizeSolution
    println(s"WeightSolver Search depth: ${allPaths.length}")
  }
  new SmartWeightLevel1Solver {
    visualizeSolution
    println(s"SmartWeightSolver Search depth: ${allPaths.length}")
  }

}
