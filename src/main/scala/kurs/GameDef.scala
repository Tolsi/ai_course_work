package kurs

trait GameDef {

  type Score

  type Block

  val start: Field

  val goal: Field

  def calculateScore(blocks: Block, count: Int): Score

  case class Selection(x: Int, y: Int)

  case class Field(private[kurs] val field: List[List[Block]]) {

    import Field._

    def select(selection: Selection): (Field, Score) = select(selection.x, selection.y)

    def select(x: Int, y: Int): (Field, Score) = {
      def selectRec(fields: Stream[List[List[Block]]], x: Int, y: Int, excited: Set[List[List[Block]]]): Stream[List[List[Block]]] = {
        if (fields.isEmpty) {
          Stream(start.field)
        } else {
          val field = fields.head
          lazy val deletedBlock = field(x)(y)

          val fieldAfterDelete = delete(field, x, y).field

          val next = Stream(
            (if (x + 1 < fieldAfterDelete.length && !fieldAfterDelete(x + 1).isEmpty && y < fieldAfterDelete(x + 1).length && fieldAfterDelete(x + 1)(y) == deletedBlock) Some((x + 1, y)) else None),
            (if (y < fieldAfterDelete(x).length && !fieldAfterDelete(x).isEmpty && fieldAfterDelete(x)(y) == deletedBlock) Some((x, y)) else None),
            (if (x - 1 >= 0 && !fieldAfterDelete.isEmpty && y < fieldAfterDelete(x - 1).size && fieldAfterDelete(x - 1)(y) == deletedBlock) Some((x - 1, y)) else None),
            (if (y - 1 >= 0 && !fieldAfterDelete(x).isEmpty && fieldAfterDelete(x)(y - 1) == deletedBlock) Some((x, y - 1)) else None)
          ).flatten

          val nexts = (for (
            step <- next;
            (x, y) = step
          ) yield delete(fieldAfterDelete, x, y).field).filter(!excited.contains(_))

          fieldAfterDelete #:: next.map {
            case (x, y) => selectRec(Stream(fieldAfterDelete), x, y, excited ++ nexts)
          }.flatten
        }
      }

      val all = selectRec(Stream(field), x, y, Set(field))
      val maxRemovedField = all.minBy(f => f.flatten.size)
      (deleteEmptyColumns(maxRemovedField), calculateScore(field(x)(y), blocksCount(field) - blocksCount(maxRemovedField)))
    }

    def moves: Seq[(Selection, (Field, Score))] =
      for (xWithIndex <- field.zipWithIndex;
           (x, xIndex) = xWithIndex;
           yWithIndex <- x.zipWithIndex;
           (y, yIndex) = yWithIndex;
           selection = Selection(xIndex, yIndex)
      ) yield (selection, select(selection))

    def movesToUniqueStates: List[(Selection, (Field, Score))] = {
      def movesToUniqueStatesRec(moves: Seq[(Selection, (Field, Score))], excited: Set[(Field, Score)]): List[(Selection, (Field, Score))] = {
        moves match {
          case Nil => Nil
          case move :: other => if (!excited.contains(move._2))
            move :: movesToUniqueStatesRec(other, excited + move._2)
          else
            movesToUniqueStatesRec(other, excited)
        }
      }
      movesToUniqueStatesRec(moves, Set())
    }

    override def toString: String = '\n' + {
      val fieldString = transpose(field).zipWithIndex.reverse.map(r => r._2 + ") " + r._1.map(b => b match {
        case Some(block) => block.toString;
        case None => ' '
      }).mkString(" ")).mkString("\n")
      if (fieldString.isEmpty)
        "Empty field"
      else
        fieldString
    } + '\n'
  }

  object Field {
    def fill(xSize: Int, ySize: Int)(gen: => Block): Field = Field(List.fill(xSize)(List.fill(ySize)(gen)))

    var empty: Field = Field(List())

    private[kurs] def delete(field: List[List[Block]], x: Int, y: Int): Field = Field(field.updated(x, field(x).take(y) ++ field(x).drop(y + 1)))

    def deleteEmptyColumns(field: List[List[Block]]): Field = Field(field.filter(!_.isEmpty))

    def blocksCount(field: List[List[Block]]): Int = field.flatten.size

    def transpose[Block](xss: List[List[Block]]): List[List[Option[Block]]] = {
      val b = new collection.mutable.ListBuffer[List[Option[Block]]]
      var y = xss.map(_.map(Option(_))) filter (!_.isEmpty)
      while (!y.forall(_.isEmpty)) {
        b += y map (_.headOption.flatten)
        y = y map (y => if (!y.isEmpty) y.tail else List())
      }
      b.toList
    }
  }

}
