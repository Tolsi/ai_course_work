package kurs

trait LengthSolver extends Solver {

  def from(initial: Stream[(List[Selection], (Field, Score))]): Stream[(List[Selection], (Field, Score))] = {
    val next = (for (
      i <- initial;
      historyAndRes <- i._2._1.moves
    ) yield from(Stream(moveWithHistory(historyAndRes, initial.head))))
    initial #::: next.flatten
  }

  lazy val allPaths = from(Stream((Nil, (start, 0))))

  lazy val pathToGoal = allPaths.filter(p => done(p._2._1))

  lazy val solution: List[Selection] = pathToGoal.maxBy(p => p._2._2)._1
}
