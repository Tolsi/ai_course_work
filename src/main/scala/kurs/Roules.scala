package kurs

trait Roules extends GameDef {
  type Score = Int

  val goal: Field = Field.empty

  def done(b: Field): Boolean = b == goal

  def calculateScore(blocks: Block, count: Int): Score = Math.pow(count, 2).toInt
}
