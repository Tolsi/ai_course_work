package kurs

trait SmartWeightSolver extends Solver {

  def newAndBetterOnlyMoves(moves: List[(Selection, (Field, Score))],
                            explored: Set[(Field, Score)]): List[(Selection, (Field, Score))] =
    moves.filter(m => !explored.map(_._1).contains(m._2._1) || explored.filter(_._1 == m._2._1).maxBy(_._2)._2 < m._2._2)

  def from(initial: Stream[(List[Selection], (Field, Score))], explored: Set[(Field, Score)]): Stream[(List[Selection], (Field, Score))] = {
    if (initial.isEmpty)
      Stream()
    else {
      val next = (for (
        i <- initial;
        historyAndRes <- newAndBetterOnlyMoves(i._2._1.movesToUniqueStates, explored)
      ) yield moveWithHistory(historyAndRes, i))
      val newExplored = explored ++ next.map(_._2)
      initial #::: from(next, newExplored)
    }
  }

  lazy val allPaths = from(Stream((Nil, (start, 0))), Set())

  lazy val pathToGoal = allPaths.filter(p => done(p._2._1))

  lazy val solution: List[Selection] = pathToGoal.maxBy(p => p._2._2)._1
}
