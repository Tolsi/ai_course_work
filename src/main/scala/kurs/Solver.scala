package kurs

abstract class Solver extends GameDef with Roules {

  def solution: List[Selection]

  def moveWithHistory(move: (Selection, (Field, Score)), history: (List[Selection], (Field, Score))): (List[Selection], (Field, Score)) =
    (history._1 :+ move._1, (move._2._1, history._2._2 + move._2._2))
}