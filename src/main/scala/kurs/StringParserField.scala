package kurs

trait StringParserField extends GameDef {

  type Block = Char

  /**
   * A ASCII representation of the field. This field should remain
   * abstract here.
   */
  val field: String

  private lazy val list: List[List[Block]] =
    List(field.split("\n").reverse.map(str => List(str: _*)): _*)

  lazy val start: Field = Field(list.transpose)

}
