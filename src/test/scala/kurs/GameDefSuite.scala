package kurs

import org.scalatest.FunSuite

class GameDefSuite extends FunSuite {

  trait GameDefChecker extends GameDef with Roules with StringParserField

  trait EasyLevel extends GameDefChecker {

    val field =
      """11
        |11""".stripMargin

  }

  trait ByOneLevel extends GameDefChecker {

    val field =
      """12
        |34""".stripMargin

  }

  trait OneBlockLevel extends GameDefChecker {

    val field =
      """1""".stripMargin

  }

  trait Level1 extends GameDefChecker {

    val field =
      """434
        |124
        |334""".stripMargin

  }

  test("parse be one level test") {
    new ByOneLevel {
      assert(start.field === List(List('3', '1'), List('4', '2')))
    }
  }

  test("select easy test") {
    new EasyLevel {
      assert(start.select(0, 0)._1 === Field.empty)
      assert(start.select(0, 0)._2 === 16)
    }
  }

  test("select by one test 1") {
    new ByOneLevel {
      assert(start.select(0, 0)._1 === Field(List(List('1'), List('4', '2'))))
      assert(start.select(0, 0)._2 === 1)
    }
  }

  test("select by one test 2") {
    new ByOneLevel {
      assert(start.select(1, 0)._1 === Field(List(List('3', '1'), List('2'))))
      assert(start.select(1, 0)._2 === 1)
    }
  }

  test("select by one test 3") {
    new ByOneLevel {
      assert(start.select(0, 1)._1 === Field(List(List('3'), List('4', '2'))))
      assert(start.select(0, 1)._2 === 1)
    }
  }

  test("select by level 1") {
    new Level1 {
      assert(start.select(0, 0)._1 === Field(List(List('1', '4'), List('2', '3'), List('4', '4', '4'))))
      assert(start.select(0, 0)._2 === 4)
    }
  }

  test("delete test 1") {
    new OneBlockLevel {
      assert(Field.deleteEmptyColumns(Field.delete(start.field, 0, 0).field) === Field.empty)
    }
  }

  test("delete test 2") {
    new ByOneLevel {
      assert(Field.delete(start.field, 0, 0).field === List(List('1'), List('4', '2')))
    }
  }

  test("delete test 3") {
    new ByOneLevel {
      assert(Field.delete(start.field, 0, 1).field === List(List('3'), List('4', '2')))
    }
  }

  test("delete by level 1") {
    new Level1 {
      assert(Field.delete(start.field, 0, 0).field === List(List('1', '4'), List('3', '2', '3'), List('4', '4', '4')))
    }
  }

  test("moves test 1") {
    new EasyLevel {
      assert(start.moves.size === 4)
    }
  }

  test("blocksCount test 1") {
    new EasyLevel {
      assert(Field.blocksCount(start.field) === 4)
    }
  }

  test("blocksCount test 2") {
    new OneBlockLevel {
      assert(Field.blocksCount(start.field) === 1)
    }
  }

  test("moves test 2") {
    new ByOneLevel {
      assert(start.moves.size === 4)
    }
  }

  test("movesToUniqueStates test 1") {
    new EasyLevel {
      assert(start.movesToUniqueStates.size === 1)
    }
  }

  test("movesToUniqueStates test 2") {
    new ByOneLevel {
      assert(start.movesToUniqueStates.size === 4)
    }
  }

  test("movesToUniqueStates test 3") {
    new Level1 {
      assert(start.movesToUniqueStates.size === 6)
    }
  }

}
