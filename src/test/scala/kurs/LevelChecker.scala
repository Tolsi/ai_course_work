package kurs

trait LevelChecker extends Solver with GameDef {
  def solve(ls: List[Selection]): Score =
    ls.foldLeft((start, 0)) {
      case (fieldAndScore, selection) => {
        val move = fieldAndScore._1.select(selection)
        (move._1, fieldAndScore._2 + move._2)
      }
    }._2
}
