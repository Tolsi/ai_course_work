package kurs

import org.scalatest.FunSuite

class SmartWeightSolverSuite extends FunSuite {

  trait SmartWeightSolverChecker extends SmartWeightSolver with LevelChecker with StringParserField

  trait EasyLevel extends SmartWeightSolverChecker {

    val field =
      """11
        |11""".stripMargin


    val optsolution = 16
  }

  trait ByOneLevel extends SmartWeightSolverChecker {

    val field =
      """12
        |34""".stripMargin


    val optsolution = 4
  }


  trait Level1 extends SmartWeightSolverChecker {

    val field =
      """434
        |124
        |334""".stripMargin


    val optsolution = 27
  }

  trait OneBlockLevel extends SmartWeightSolverChecker {

    val field =
      """1""".stripMargin


    val optsolution = 1
  }

  test("solution for easy level") {
    new EasyLevel {
      assert(solution === List(Selection(0, 0)))
    }
  }

  test("solution for by one level") {
    new ByOneLevel {
      assert(solution === List(Selection(0, 0), Selection(0, 0), Selection(0, 0), Selection(0, 0)))
    }
  }

  test("solution for level 1") {
    new Level1 {
      assert(solution === List(Selection(0, 1), Selection(1, 1), Selection(0, 0), Selection(0, 0)))
    }
  }


  test("optimal solution for easy level") {
    new EasyLevel {
      assert(solve(solution) === optsolution)
    }
  }

  test("optimal solution for by one level") {
    new ByOneLevel {
      assert(solve(solution) === optsolution)
    }
  }

  test("optimal solution for level 1") {
    new Level1 {
      assert(solve(solution) === optsolution)
    }
  }

}
